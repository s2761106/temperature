package com.example.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConverterTest {

    @Test
    void testValues(){
        Converter converter = new Converter();
        Assertions.assertEquals(50, converter.convert(10));
        Assertions.assertEquals(68, converter.convert(20));
        Assertions.assertEquals(41, converter.convert(5));
    }

}
