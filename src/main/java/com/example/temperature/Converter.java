package com.example.temperature;

public class Converter {

    public double convert(double celcius){
        return celcius*1.8+32;
    }

}
